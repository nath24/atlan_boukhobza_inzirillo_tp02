import * as deepl from 'deepl-node';

const authKey = "af4fd58d-6549-6099-0ddf-e47e1575f8c5:fx"; // Replace with your key
const translator = new deepl.Translator(authKey);

(async () => {
    const result = await translator.translateText('Hello, world!', null, 'fr');
    console.log(result.text); // Bonjour, le monde !
})();

console.log(translations[1].text); // 'How are you?'
console.log(translations[1].detectedSourceLang); // 'es'

const translations = await translator.translateText(
    ['お元気ですか？', '¿Cómo estás?'],
    null,
    'en-GB',
);

const usage = await translator.getUsage();
if (usage.anyLimitReached()) {
    console.log('Translation limit exceeded.');
}
if (usage.character) {
    console.log(`Characters: ${usage.character.count} of ${usage.character.limit}`);
}
if (usage.document) {
    console.log(`Documents: ${usage.document.count} of ${usage.document.limit}`);
}
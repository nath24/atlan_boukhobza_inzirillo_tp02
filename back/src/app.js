import fastifyCookie from '@fastify/cookie'
import fastify from 'fastify'
import fastifyCors from 'fastify-cors'
import helmet from 'fastify-helmet'
import ws from 'fastify-websocket'
import { addNewUserRoute } from './routes/addNewChat.js'
import { chatRoutes } from './routes/chat.js'
import { chatHistoryRoute } from './routes/chatHistory.js'
import DeeplRoutes from './routes/DeeplRoutes.js'
import { getConversationRoute } from './routes/GetConversations.js'
import { userRoutes } from './routes/signin.js'
import { updateUserRoute } from './routes/updateUser.js'
/**
 * @param { import('fastify').FastifyServerOptions } options
 */

export function build(options = {}) {
  const app = fastify(options) // Top
  app.register(fastifyCors, { 
    origin: (origin, cb) => {
      const hostname = new URL(origin).hostname
      if(hostname === "localhost"){
        //  Request from localhost will pass
        cb(null, true)
        return
      }
      // Generate an error on other origins, disabling access
      cb(new Error("Not allowed"), false)
    }
  })
  app.register(helmet) // Enregistrer les routes //helmet => ?
  app.register(ws) // Enregistrer les routes //ws => ?
  app.register(fastifyCookie)
  app.get('/', (request, reply) => {
    reply.send({ message: 'Hello world' }) // Message d'accueil
  })
  app.register(getConversationRoute)
  app.register(chatHistoryRoute)
  app.register(userRoutes)
  app.register(addNewUserRoute)
  app.register(chatRoutes, { prefix: '/chat' }) //history => /chat/history*
  app.register(DeeplRoutes) // /Deepl => /Deepl/Deepl*
  app.register(updateUserRoute) // /Deepl => /Deepl/Deepl*
  return app
}

import { randomUUID } from 'crypto'
import { send } from 'process'
import { createAccount, getMessages, writeMessageData } from '../firebase.js'

/**
 * @typedef {Object} Message
 * @property {string} id - an uuid
 * @property {Date} sentAt - date the mesage was sent
 * @property {string} pseudo - sender pseudo
 * @property {string} body - body of the message
 */
/** @type { Message[] } */
const messages = []

/**
 * @param {string} pseudo
 * @param {string} body
 */

function handleNewMessage(pseudo, body,receiver) {
  const message = {
    id: randomUUID(),
    sentAt: new Date(),
    pseudo,
    body,
    receiver
  }
  messages.push(message)
  return message
}

/**
 * @type { import('fastify').FastifyPluginCallback }
 */
export async function chatRoutes(app) {
  /**
   * @param {{ type: string, payload: object }} data
   */
  function broadcast(data) {//?
    app.websocketServer.clients.forEach((client) => {//?
      client.send(JSON.stringify(data))
    })
    writeMessageData({...data})
  }

  app.get('/', { websocket: true }, (connection, req) => {
    connection.socket.on('message', (message) => {//?
      connection.socket.send('hello')
      const data = JSON.parse(message.toString('utf-8'))
      broadcast({
        type: 'NEW_MESSAGE',
        payload: handleNewMessage(data.pseudo, data.body,data.receiver),
      })
    })
  })

  
  
  
}
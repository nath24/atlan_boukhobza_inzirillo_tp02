import { getMessages } from "../firebase.js";

export async function chatHistoryRoute(app){
    app.get('/history', async (request, reply) => {
        const sender = request.query.sender;
        const receiver = request.query.receiverName
        console.log(sender)
        const messages =await getMessages(sender,receiver)
        await console.log("messages",messages)
        reply.send([...messages])
    })
}
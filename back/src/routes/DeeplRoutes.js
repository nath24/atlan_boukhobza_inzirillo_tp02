import * as deepl from 'deepl-node';

const authKey = "af4fd58d-6549-6099-0ddf-e47e1575f8c5:fx"; // Replace with your key
const translator = new deepl.Translator(authKey);

async function DeeplRoutes(app) {
    app.get('/Deepl', async (request, reply) => {
        async function translate() {
            const result = await translator.translateText('Consumer pressure', null, 'fr');
            console.log(result.text); // Bonjour, le monde !
        }
        translate();
    });
}
export default DeeplRoutes;
//import {fastifySession} from '@fastify/secure-session'
/**
 * @typedef {Object} User
 * @property {string} id - an uuid
 * @property {Date} sentAt - date the mesage was sent
 * @property {string} pseudo - sender pseudo
 * @property {string} password - body of the message
 */

import { initializeApp } from 'firebase/app';
import { doc, getFirestore, updateDoc } from 'firebase/firestore';
const firebaseConfig = {
  apiKey: "AIzaSyCIAom5kfAmlfAVI2X9tDuxQxuVMcXm3FQ",
  authDomain: "messenger-c16b7.firebaseapp.com",
  projectId: "messenger-c16b7",
  storageBucket: "messenger-c16b7.appspot.com",
  messagingSenderId: "801942153819",
  appId: "1:801942153819:web:930a251ebf30724ddb2d7e",
  measurementId: "G-VT78EKM790"
};
/** @type { User[] } */
const users = []

/**
 * @param {string} pseudo
 * @param {string} password
 */


/**
 * @type { import('fastify').FastifyPluginCallback }
 */
export async function updateUserRoute(app){
     /**
   * @param {{ type: string, payload: object }} data
   */
  app.post('/updateUser',async (request, reply) => {
    const app = initializeApp(firebaseConfig);
    const db = getFirestore(app)
    const image = request.query.photo;
    const id = request.query.id;
    const userRef = doc(db, "User", id);
    // Set the "capital" field of the city 'DC'
    await updateDoc(userRef, {
      Avatar:image
    });
    reply.send("Update successful with : "+image + " for " +id)
  })
}
import { createAccount, userConnection } from '../firebase.js'
import { JWT } from '../jwt_helper.js';
//import {fastifySession} from '@fastify/secure-session'
/**
 * @typedef {Object} User
 * @property {string} id - an uuid
 * @property {Date} sentAt - date the mesage was sent
 * @property {string} pseudo - sender pseudo
 * @property {string} password - body of the message
 */
/** @type { User[] } */
const users = []

/**
 * @param {string} pseudo
 * @param {string} password
 */


/**
 * @type { import('fastify').FastifyPluginCallback }
 */
export async function userRoutes(app) {
  /**
   * @param {{ type: string, payload: object }} data
   */

  
   app.post('/newUser', (request, reply) => {
    const username = request.query.username;
    const password = request.query.password;
    console.log(username);
    createAccount(username, password)
    reply.send({
      status:"inscription successful",
      name:username,
      id:undefined,
      PhotoDeProfil:"https://cdn.dribbble.com/userupload/2789857/file/original-9cafa3ae667ca5c9351187d2dbdf5e4a.png?compress=1&resize=1504x1128",
      Messages:undefined,
      chat: undefined
    });
  })
  app.get('/login', async (request, reply) => {
    console.log("tried")
    const username = request.query.username;
    const password = request.query.password;
    const userData = await userConnection(username, password)
    await console.log(userData.data())
    
    const token = await JWT(userData.id)
    await console.log(token)
    reply.send({
      name:userData.data().pseudo,
      id:userData.id,
      PhotoDeProfil:userData.data().Avatar,
      Messages:undefined,
      chat: undefined,
      token:token,
      isConnected:true
    })
    
  })
 
}
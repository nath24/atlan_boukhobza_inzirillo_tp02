//import {fastifySession} from '@fastify/secure-session'
/**
 * @typedef {Object} User
 * @property {string} id - an uuid
 * @property {Date} sentAt - date the mesage was sent
 * @property {string} pseudo - sender pseudo
 * @property {string} password - body of the message
 */

 import { initializeApp } from 'firebase/app';
 import { collection, doc, getDoc, getDocs, getFirestore, onSnapshot, query, setDoc, updateDoc, where } from 'firebase/firestore';
 const firebaseConfig = {
   apiKey: "AIzaSyCIAom5kfAmlfAVI2X9tDuxQxuVMcXm3FQ",
   authDomain: "messenger-c16b7.firebaseapp.com",
   projectId: "messenger-c16b7",
   storageBucket: "messenger-c16b7.appspot.com",
   messagingSenderId: "801942153819",
   appId: "1:801942153819:web:930a251ebf30724ddb2d7e",
   measurementId: "G-VT78EKM790"
 };
  const fire = initializeApp(firebaseConfig);
  
 
 /**
  * @type { import('fastify').FastifyPluginCallback }
  */
 export async function getConversationRoute(app){
      /**
    * @param {{ type: string, payload: object }} data
    */
   app.get('/getConversations',async (request, reply) => {
     //userID
     console.log("Everything done")
     const db = getFirestore(fire)
     const myId = request.query.id;
     //Obtenir l'id de l'utilisateur 2
     const userRef = collection(db, "User",myId,"Discussions");
     const q1 = query(userRef);
     const querySnapshot = await getDocs(q1);
     let userInformation = []
     querySnapshot.forEach((doc) => {
       userInformation.push({data:doc.data(),id:doc.id})
       console.log(doc.data())
     });
     //console.log(userInformation)
     
     console.log("Everything done")
     //Créer une nouvelle collection sur l'utilisateur 1 avec les informations de l'utilisateur 2
     //await getDoc(userRef1);
     reply.send({status:'done',liste:userInformation})
     
   })
 }
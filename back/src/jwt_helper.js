import jsonwebtoken from 'jsonwebtoken'
import dotenv from 'dotenv'
export async function JWT(userId){
  dotenv.config()
  let token=jsonwebtoken.sign({ id: userId },process.env.secretKey ,  { expiresIn: '24h' })
  console.log(token)
  return token
}

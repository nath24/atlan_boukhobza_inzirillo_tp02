// Import the functions you need from the SDKs you need
import { randomUUID } from "crypto";
import bcrypt from "bcryptjs/dist/bcrypt.js";
import { initializeApp } from "firebase/app";
import { getFirestore, doc, setDoc, query, where, collection, getDocs, getDoc, onSnapshot } from "firebase/firestore"
import jsonwebtoken from 'jsonwebtoken'
import dotenv from 'dotenv'
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
export const firebaseConfig = {
  apiKey: "AIzaSyCIAom5kfAmlfAVI2X9tDuxQxuVMcXm3FQ",
  authDomain: "messenger-c16b7.firebaseapp.com",
  projectId: "messenger-c16b7",
  storageBucket: "messenger-c16b7.appspot.com",
  messagingSenderId: "801942153819",
  appId: "1:801942153819:web:930a251ebf30724ddb2d7e",
  measurementId: "G-VT78EKM790"
};
//LucaInzi => ad292e21-b4b1-4533-93fd-3f239bb4859a
//Ezequiel => 168e58af-d4f6-4238-bbad-9d66b6bc256d

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
// Add a new document in collection "cities"
  export function writeMessageData(data) {
    const db = getFirestore(app) // const app = initializeApp(fireConfig)
    const messageRef = doc(db, 'Message', randomUUID())
    //console.log(data.payload.sentAt)
    setDoc(messageRef, {
      sender: data.payload.pseudo,
      message: data.payload.body,
      receiver: data.payload.receiver,
      Date: Math.round(+new Date() / 1000),
    })
  }
  export function createAccount(username, password,avatar="https://cdn.dribbble.com/users/1731254/screenshots/16912619/media/db24231bfbbb4e11502bf1d66c4b8e98.png?compress=1&resize=1200x900&vertical=top") {
    var hash = bcrypt.hashSync(password, 8);
    console.log("User connection tried")
    const db = getFirestore(app) // const app = initializeApp(fireConfig)
    const UserRef = doc(db, 'User', randomUUID())    
    setDoc(UserRef, {
        Avatar:avatar,
        pseudo: username,
        password:hash,
        Discussions:[]
    })   
  }
  export async function getMessages(sender,receiver) {
    const db = getFirestore(app) // const app = initializeApp(fireConfig)
    //const messageRef = doc(db, 'Message')
    
    //
    console.log("getMessage 1")
    console.log(sender)
    console.log(receiver)
    const UserRef = collection(db, 'User')
    const messageRef = collection(db,"Message")
    const receiverName = await query(messageRef, where("receiver", "==", receiver), where("sender", "==", sender));
    const receiverName2 = await query(messageRef, where("receiver", "==", sender), where("sender", "==", receiver));
    //onsole.log(receiverName)
    const querySnapshot2 = await getDocs(receiverName)
    const querySnapshot1 = await getDocs(receiverName2)
    //await console.log(querySnapshot2)
    let listeMessages = []
    await querySnapshot2.forEach((doc) => {
      listeMessages.push(doc.data())
  });
  await querySnapshot1.forEach((doc) => {
    listeMessages.push(doc.data())
});
  await console.log('listeMessages',listeMessages)
  return listeMessages
  /*listeMessages=listeMessages.sort((a,b)=>{
    return a.Date - b.Date
  })*/
  }
  export async function userConnection(username, password) {
    console.log('hello world')
    const db = getFirestore(app) // const app = initializeApp(fireConfig)
    const userRef = collection(db, "User");
    console.log("Hello world")
    const q1 = query(userRef, where("pseudo", "==", username));
    const querySnapshot = await getDocs(q1);
    let userInformation;
    querySnapshot.forEach((doc) => {
      userInformation = doc;
    });
    const isPassword = await bcrypt.compare(password, userInformation.data().password)
    await console.log("isPassword", isPassword)
    if(isPassword===true)
    {
      return userInformation
    }
  }
  /*
    const db = getDatabase();
    set(ref(db, 'Message/' + data.payload.id), {
      message: data.payload.body,
      Utilisateur: data.payload.pseudo

      .then((isTrue)=> {
        console.log("isPasswordTrue ? ", isTrue)
        userData.pseudo = doc.data().pseudo,
        userData.avatar = doc.data().Avatar,
        userData.isConnected = isTrue
        userData.id = doc.id()
      })
  })*/
  
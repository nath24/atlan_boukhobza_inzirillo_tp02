import React, { useContext, useEffect, useState } from "react";
import Button from "../Component/Button";
import Input from "../Component/Input";
import { AuthContext } from "../Memory/Memory";
export default function Register() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [passwordMessage, setPasswordMessage] = useState("Quel est votre mot de passe ?");
  const { connect, authFunctions } = useContext(AuthContext);
  useEffect(()=>{
    if(password.length <8){
        setPasswordMessage("Met un bon mdp stp") 
    }
    else if(8<password.length&& password.length <12)
    {
    setPasswordMessage("Le mot de passe est correct ")
    }
    else if (password.length >12){
    setPasswordMessage("Le mot de passe est fort ")
    } 
  },[password])
  useEffect(()=>{
    console.log(authFunctions.user)
  },[authFunctions.isConnected])
  async function handleSubmit(event) {
    event.preventDefault();
    const link =
      "http://localhost:5000/login?username=" +
      username +
      "&password=" +
      password;
    
    const userData = await fetch(link, {
    method: "GET",
    }).then((response)=>{return response.json()})
    
    await connect(userData)
    
  }
  return (
    <form className="Input-list" >
     
      <Input  
        placeholder="Username"
        type="text"
        onChange={(e) => setUsername(e.target.value)}
        value={username}
      />
    
      <Input
        placeholder="password"
        type="password"
        onChange={(e) => setPassword(e.target.value)}
        value={password}
      />
      <p>{passwordMessage}</p>

      <Button className="Button-Continue" handleSubmit={handleSubmit} />
    </form>
  );
}
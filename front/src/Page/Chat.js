import React, { useEffect } from "react"
import '../App.css';
import Chatting from '../Component/UsersChat/Chatting';
import Header from "../Component/Header";
import { main } from "../ws/main";
import { initChat } from "../ws/chat";

export default function Chatfriends() {
    useEffect(()=>{
        main()
        initChat()
    },[])
    return (     

        <div>
            <Header></Header>
            <Chatting/>      
        </div>
    )
}
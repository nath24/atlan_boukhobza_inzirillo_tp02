import { useContext, useEffect, useState } from 'react'
import PhotoDeProfil from '../../../front/src/Component/PhotoDeProfil'
import PhotoDeProfilCadeau from '../../../front/src/Component/PhotoDeProfilCadeau'
import ProgressBar from '../../../front/src/Component/ProgressBar'
import { AuthContext } from '../Memory/Memory'
export default function Profil() {
    const {authFunctions} = useContext(AuthContext)
    const [imageUrl, modifierImage] = useState(authFunctions.user.PhotoDeProfil)
    useEffect(()=>{
        alert(authFunctions.user.name)
    },[])
    function modifierImageUrl(url,){
        let id = authFunctions.user.id
        modifierImage(url);
        fetch('http://localhost:5000/updateUser?photo='+url+"&id="+id,{method:"POST"})
    }
    const [niveauSelectionné, modifierNiveau] = useState()
    return (
    <div className='Profil'>
        <div>
            <PhotoDeProfil style="photoDeProfil" src={imageUrl}/>
            <h3>{authFunctions.user.name}</h3> 
            <h4>Hokage de Konoha</h4> 
        </div>  
        <p>niv 12 - 2/5 missions</p>
        <ProgressBar/>
        <h4>Changer de photo</h4>
        <div className='ScrollableContainerPhoto'>
            <PhotoDeProfilCadeau niveau={11} src='https://cdn.dribbble.com/users/2119910/screenshots/15895749/media/20db3d04f1fb07872e46a2ff7ab110e1.png' onClick={()=>modifierImageUrl('https://cdn.dribbble.com/users/2119910/screenshots/15895749/media/20db3d04f1fb07872e46a2ff7ab110e1.png')|| modifierNiveau(11)} niveauSelectionne={niveauSelectionné}/>
            <PhotoDeProfilCadeau niveau={12} src='https://cdn.dribbble.com/userupload/2789857/file/original-9cafa3ae667ca5c9351187d2dbdf5e4a.png'  onClick={()=>modifierImageUrl('https://cdn.dribbble.com/userupload/2789857/file/original-9cafa3ae667ca5c9351187d2dbdf5e4a.png')|| modifierNiveau(12)} niveauSelectionne={niveauSelectionné}/>
            <PhotoDeProfilCadeau niveau={7} src='https://cdn.dribbble.com/users/334862/screenshots/15593779/media/0e46dee7ecb90a48d6c63ec2f2b365db.png' onClick={()=>modifierImageUrl('https://cdn.dribbble.com/users/334862/screenshots/15593779/media/0e46dee7ecb90a48d6c63ec2f2b365db.png') || modifierNiveau(7)} niveauSelectionne={niveauSelectionné}/>
            <PhotoDeProfilCadeau niveau={8} src='https://cdn.dribbble.com/users/2598141/screenshots/14875252/media/1d0e3b9b54975065a081aa26107a97b8.png' onClick={()=>modifierImageUrl('https://cdn.dribbble.com/users/2598141/screenshots/14875252/media/1d0e3b9b54975065a081aa26107a97b8.png') || modifierNiveau(8)} niveauSelectionne={niveauSelectionné}/>
            <PhotoDeProfilCadeau niveau={9} src='https://cdn.dribbble.com/users/1051664/screenshots/5725969/media/4629b6af1b9ea710759307a22d69f546.jpg' onClick={()=>modifierImageUrl('https://cdn.dribbble.com/users/1051664/screenshots/5725969/media/4629b6af1b9ea710759307a22d69f546.jpg') || modifierNiveau(9)} niveauSelectionne={niveauSelectionné}/>
            <PhotoDeProfilCadeau niveau={10} src='https://cdn.dribbble.com/users/4018190/screenshots/7159861/media/86e22676d4f81ef96b775cc0ca109662.jpg' onClick={()=>modifierImageUrl('https://cdn.dribbble.com/users/4018190/screenshots/7159861/media/86e22676d4f81ef96b775cc0ca109662.jpg') || modifierNiveau(10)} niveauSelectionne={niveauSelectionné}/>
        </div>
    </div>

  )
}

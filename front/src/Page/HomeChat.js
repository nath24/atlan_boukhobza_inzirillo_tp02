import React, { useContext, useEffect, useState } from "react"
import '../App.css';    
import Header from "../Component/Header";
import Previewchat from "../Component/ListChat/PreviewChat";
import { AuthContext } from "../Memory/Memory";

export default function HomeChat() {

    const [data, updateData]= useState()
    const {authFunctions} = useContext(AuthContext)
    const link = "http://localhost:5000/getConversations?id="+authFunctions.user.id
    async function getDiscussion() {
        updateData(await fetch(link, {
        method: "GET",
        }).then((response)=>{return response.json()}))
    }
    useEffect(()=>{
        getDiscussion()
    },[])
    useEffect(()=>{
        console.log(data)
    },[data])
    return (     
        <div>
            <Header></Header>
            {data === undefined ?<Previewchat name={"Madara"}/>: data.liste.map((element)=><Previewchat name={element.data.pseudo}/>)}
        </div>
    )
}

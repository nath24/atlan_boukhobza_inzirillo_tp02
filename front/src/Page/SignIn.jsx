import React, { useState } from 'react'
import Button from '../Component/Button'
import Input from '../Component/Input'
export default function SignIn() {
    const [username, setUsername] = useState()
    const [password, setPassword] = useState()

    function handleSubmit(event) {
      event.preventDefault();
      const link = 'http://localhost:5000/chat/newUser?username='+username+"&password="+password;
      console.log(username)
      fetch(link, {
        method: "POST",
        body: JSON.stringify({pseudo:username, password:password})
      })
    }
  return (
    <form>
        <Input placeholder='Username' type="text" onChange={(e)=>setUsername(e.target.value)} value={username}/>
        <Input placeholder='password' type="password" onChange={(e)=>setPassword(e.target.value)} value={password}/>
        <Button handleSubmit={handleSubmit}/>
    </form>
  )
}

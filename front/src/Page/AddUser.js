import react, { useContext, useEffect, useState } from 'react'
import { useNavigate } from "react-router-dom";
import { AuthContext } from '../Memory/Memory';
export function AddUser(){
    const [username, setUserName] = useState()
    const [message, setMessage] = useState()
    const [Information, setInformation] = useState({status:"empty"})
    const {authFunctions} = useContext(AuthContext)
    useEffect(()=>{
        if(Information.status==='done'){
            setMessage('Vous allez être redirigé ...')
            navigate('../Chat?username='+username)
        }
        else if(Information.status==='done') {
            setMessage("L'utilisateur n'existe pas")
        }
        else{
            setMessage("Connexion en cours ...")
        }
    },[Information])
    let navigate = useNavigate()
    return (
        <form className='Input-list'>
            <input className='Input-Add' placeholder="Tapez le nom de l'utilisateur" onChange={(e)=>setUserName(e.target.value)} value={username}/>
            <button className='Button-Add' onClick={async (e)=>{
                e.preventDefault()
                setInformation(await fetch('http://localhost:5000/addNewChat?username='+username+"&id="+authFunctions.user.id,
                {method:"GET"})
                .then((data)=>{return data.json()}))
            }}>Chercher</button>
            <p>{message}</p>
        </form>
    )
}
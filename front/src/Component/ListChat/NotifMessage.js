import './../../App.css'

function NotifMessage({number}) {
    return (
        <div className='BubbleNotif'>
          <p className='NumberNotif'> {number} </p>
          </div>
    );
  }
  export default NotifMessage;
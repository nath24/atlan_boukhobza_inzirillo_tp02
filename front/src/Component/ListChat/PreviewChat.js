import './../../App.css'
import NameFriend from './NameFriend';
import LastMessage from './LastMessage';
import DateMessage from './DateMessage';
import NotifMessage from './NotifMessage';
import { Link } from 'react-router-dom';
import { useEffect } from 'react';




function Previewchat({name}) {
  useEffect(()=>{
    console.log('name : '+name)
  },[])
  return (
    <Link to={"/Chat/"+name} className='linki'>
    <div className='Preview' >
        <img className='Imgchat' src='https://upload.wikimedia.org/wikipedia/commons/thumb/6/64/Mangekyou_Sharingan_Madara_%28Eternal%29.svg/2048px-Mangekyou_Sharingan_Madara_%28Eternal%29.svg.png' alt='imgmadara'/>
        <div className='Name-Message'>
        <NameFriend name={name}></NameFriend>
        <LastMessage message={"Gros je crois j'ai perdu un sharingan"}></LastMessage>
        </div>
        <div className='Notif-Date'>
        <DateMessage date={"12:07 PM"}></DateMessage>
        <NotifMessage number={"2"}></NotifMessage>
        </div>
        
    </div>
    </Link> 
  );
}
export default Previewchat;

import ChatReceived from "./ChatReceived";
import DateChatReceived from "./DateChatReceived";

export function Receiver({message}){
    return (
        <div className='Message-Received'>
            <ChatReceived message={message}/>
            <DateChatReceived  date={"12:08 PM"} className='Message-send'></DateChatReceived>
        </div>
    )
}
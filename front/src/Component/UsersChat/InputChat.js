import { useContext, useEffect, useState } from 'react'
import { AuthContext } from '../../Memory/Memory';
import { sendMessage } from '../../ws/chat';
import './../../App.css'

function InputChat({name}) {
  const {authFunctions} = useContext(AuthContext)
  const ws = new WebSocket('ws://localhost:5000/chat');
  const base = 'http://localhost:5000'
  const [pseudo,updatePseudo] = useState(authFunctions.user.name)
  const [Receiver,updateReceiver] = useState(name)
  
  const [message,updateMessage] = useState()

    return (
    <form className='EditMessage'>    
      <input className='InputChat' value={message} onChange={(e)=>{updateMessage(e.target.value)}}/>
         <button className='ButtonEnvoyer' onClick={(e)=>{
          sendMessage(e,pseudo,message,Receiver)
          console.log("name : "+name)
          updateMessage("")
          }}> envoyer </button>
    </form> 
    );
  }
  export default InputChat;
import { useContext, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { AuthContext } from '../../Memory/Memory';
import './../../App.css'
import ChatReceived from './ChatReceived';
import ChatSend from './ChatSend';
import DateChatReceived from './DateChatReceived';
import DateChatSend from './DateChatSend';
import InputChat from './InputChat';
import { MessageSend } from './MessageSend';
import { Receiver } from './Receiver';


function Chatting() {
  let  {username}  = useParams();
  const [data, updateData]= useState()
    const {authFunctions} = useContext(AuthContext)
    const link = "http://localhost:5000/history?sender="+authFunctions.user.name+"&receiverName="+username
    async function getDiscussion() {
        updateData(await fetch(link, {
        method: "GET",
        }).then((response)=>{return response.json()}))
    }
    useEffect(()=>{
        getDiscussion()
    },[])
    useEffect(()=>{
      console.log("data",data)
  },[data]) 
  return (
    <div className='chatting' >
      {Array.isArray(data)?data.map((message)=>{
        return (message.sender===authFunctions.user.name ? <MessageSend message={message.message}/>:<Receiver message={message.message}/>)}
      ):<MessageSend message={"Gros je crois j'ai perdu un sharingan"}/>}
        
        
        <InputChat name={username}></InputChat>
    </div>

  );
}
export default Chatting;
import ChatSend from "./ChatSend";
import DateChatSend from "./DateChatSend";

export function MessageSend({message}){
    return(
        <div className='Message-send'>
            <ChatSend message={message}></ChatSend>
            <DateChatSend date={"12:07 PM"}></DateChatSend>
        </div>
    )
}
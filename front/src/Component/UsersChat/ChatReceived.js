import './../../App.css'

function ChatReceived({message}) {
    return (
        <div className='BubbleChatReceived'>
          <p className='ChatReceived'> {message} </p>
        </div>
    );
  }
  export default ChatReceived;
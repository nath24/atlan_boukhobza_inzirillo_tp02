import React from 'react'

export default function Input({placeholder,type, value, onChange}) {
  return (
    <div>
        <input placeholder={placeholder} className="Input-Username" type={type} value={value} onChange={onChange}/>
    </div>
  )
}

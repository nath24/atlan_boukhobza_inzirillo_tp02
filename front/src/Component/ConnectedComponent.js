import './../App.css'
import {BrowserRouter, Routes, Route,} from "react-router-dom";
import HomeChat from '../Page/HomeChat';
import Chat from '../Page/Chat';
import Register from '../Page/Register';
import AppLayout from '../Component/AppLayout'
import Profil from '../Page/Profil.jsx';
import { useContext } from 'react';
import { AuthContext } from '../Memory/Memory';
import { AddUser } from '../Page/AddUser';
export function ConnectedComponent(){
    const {isConnected} = useContext(AuthContext)
    return(
    <>
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<Register />} />
                <Route path="HomeChat" element={<HomeChat />} />
                <Route path="Profil" element={<Profil />} />
                <Route path="Login" element={<Register />}/>
                <Route path="addUser" element={<AddUser />}/>
                <Route path="Chat/:username" element={<Chat />}/>
            </Routes>
            <AppLayout/>
        </BrowserRouter>
    </>
    )
  }
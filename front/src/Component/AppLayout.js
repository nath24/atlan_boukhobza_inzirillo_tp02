import * as React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Container from '@mui/material/Container';
import { Outlet } from 'react-router-dom';
import Navbar from './NavBar';

export default function AppLayout() {
  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="sm">
        <Outlet/>
        <Navbar/>
      </Container>
    </React.Fragment>
  );
}
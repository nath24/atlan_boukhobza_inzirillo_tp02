import React, { useEffect, useState } from 'react'

export default function PhotoDeProfilCadeau({src,niveau, onClick, niveauSelectionne}) {
  return (
    <div className={niveauSelectionne===niveau?'photoDeProfilCadeauContainerSélectionné':'photoDeProfilCadeauContainer'} onClick={onClick}>
        <img className='photoDeProfilCadeau' src={src}/>
        <div className='barBlanche'>
            <p>niv {niveau} et {niveauSelectionne}</p>
        </div>
    </div>
  )
}


import '../App.css';
import { Link } from "react-router-dom";
import { BottomNavigation, BottomNavigationAction, SvgIcon } from '@mui/material';
import { Avatar } from '@mui/material';
import Chat from '../Icones/Chat.svg'
import { useContext } from 'react';
import { AuthContext } from '../Memory/Memory';






function Navbar() {
  const { isConnected, authFunctions } = useContext(AuthContext)
  return (
    <>
    <div className='NavBot'>
    <BottomNavigation showLabels>
        <BottomNavigationAction label="Chat" component={Link} to="/HomeChat" icon={<Avatar src={Chat} />}/>
        <BottomNavigationAction label="Profil" component={Link} to="/Profil" icon={<Avatar alt="Remy Sharp" className='PPNav' src={authFunctions.user.PhotoDeProfil} /> } /> 
        {isConnected ? <BottomNavigationAction label="Add chat" component={Link} to="/addUser" icon={<SvgIcon></SvgIcon>} />:<BottomNavigationAction label="login" component={Link} to="/Login" icon={<SvgIcon></SvgIcon>} />}
    </BottomNavigation>
    </div>
    </>
  );
}
export default Navbar;

import React from 'react'

export default function Button({handleSubmit}) {
    
    
  return (
    <button className='Button-Continue' type='submit' onClick={event =>handleSubmit(event)}>Continuer</button>
  )
}

import React from 'react'

export default function PhotoDeProfil({style,src="https://cdn.dribbble.com/userupload/2789857/file/original-9cafa3ae667ca5c9351187d2dbdf5e4a.png?compress=1&resize=2400x1800"}) {
  return (
    <img className={style} alt="Photo de profil" src={src}/>
  )
}

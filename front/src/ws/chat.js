import { fetchAPI } from './api'
import { appendMessage } from './dom'

/** @param {MessageEvent} event */
function handleWSMessage(event) {
    console.log(data)
  const data = JSON.parse(event.data)

  if (data?.type === 'NEW_MESSAGE') {
    appendMessage(data.payload)
  }
}

const ws = new WebSocket('ws://localhost:5000/chat')
ws.onopen = function open() {
  console.log('ws connected')
}
ws.onmessage = handleWSMessage

async function fetchHistory() {
  const messages = await fetchAPI('/chat/history')
  console.log("Hello world fetch History")
}

export function initChat() {
  fetchHistory()

}
export function sendMessage(event,pseudo, body, receiver){
        event.preventDefault()
        const newMessage = { pseudo, body, receiver }
        ws.send(JSON.stringify(newMessage))
}
import { createContext, useState } from "react";

/*
        Connecté
        Déconnecté

    >---------------------------------- <

        Prénom,
        Id,
        PhotoDeProfil 
        Messages
*/
/*
Commandes globales: 
    Reset
    Login 

Commandes user:
    -> Changer la photo de profil 
    -> Ajouter une conversation 
*/


export const AuthContext = createContext("light");

export default function AuthProvider({ children }) {
    const [isConnected, updateStatus] = useState(false);
    const [username, editUsername] = useState("Quel est votre nom ?");
    const [password, editPassword] = useState();
    const [user, updateUser] = useState({
        isConnected: false,
        name:undefined,
        id:undefined,
        PhotoDeProfil:undefined,
        Messages:undefined,
        chat: undefined
    })
    function resetUser(){
        updateUser({
            isConnected: false,
            name:undefined,
            id:undefined,
            PhotoDeProfil:undefined,
            Messages:undefined,
            chat:undefined
        })
    }
    function connectionAttempt(username, password){
        fetch('https://localhost:3000/connectionAttempt?username='+username+'&password='+password, {method:"GET"})
        .then((data)=>{
            if(data.status === "connection successfull" || "inscription successful"){//Regarder la sécurité de cette méthode, auth avec id et token ?
                updateStatus(true)
                updateUser({
                    isConnected: isConnected,
                    name:data.name,
                    id:data.id,
                    PhotoDeProfil:data.photo,
                    Messages:{...data.message},
                    chat: {...data.chat}
                })
            }
        })
    }
    const connect =(data)=>{
        console.log("traitement...")
        console.log(data)
        if(data.isConnected){
            updateStatus(data.isConnected)
            updateUser({
                isConnected: isConnected,
                name:data.name,
                id:data.id,
                PhotoDeProfil:data.PhotoDeProfil,
                Messages:{...data.message},
                chat: {...data.chat}
            })
            console.log(user)
            console.log("connexion effectuée")
        }
    }
    const authFunctions={
        isConnected:isConnected,
        username:username,
        editUsername:editUsername,
        editPassword:editPassword,
        connection:connect,
        user:user, 
        updateIC:updateStatus,
    }
  return (
    <AuthContext.Provider value={{ isConnected, authFunctions,connect }}>
      {children}
    </AuthContext.Provider>
  );
}

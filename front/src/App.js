
import { useEffect } from 'react';
import { ConnectedComponent } from './Component/ConnectedComponent';
import AuthProvider, { AuthContext } from './Memory/Memory';
import Register from './Page/Register';

// import your route components too

function App() {
  return (
    <div>
      <AuthProvider>
        <AuthContext.Consumer>
          {({isConnected, authFunctions, connect})=>(
            isConnected ? <ConnectedComponent/> : <Register/>
          )}
        </AuthContext.Consumer>
      </AuthProvider>
    </div>
  );
}
export default App;
